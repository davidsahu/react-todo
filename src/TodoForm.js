import React from "react";
import TextField from "@material-ui/core/TextField";

 const TodoForm = ({value, addTask, inputValues }) => (
    <form
      onSubmit={e => {
        e.preventDefault();
        addTask();
      }}
    >
      <TextField
        type="text"
        id="standard-name"
        label="Add to-do..."
        margin="normal"
        value={value}
        onChange={inputValues}
      />
    </form>
 );

export default TodoForm;
