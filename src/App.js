import React from "react";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      todos: []
    };
  }

  inputValues = e => {
    this.setState({ value: e.target.value });
  };

  addTask = () => {
    if (this.state.value) {
      this.setState({
        todos: [
          ...this.state.todos,
          {
            value: this.state.value,
            isComplete: false
          }
        ],
        value: ""
      });
    }
    console.log(this.state);
  };

  toggleCompleted = index => {
    const todos = [...this.state.todos];
    todos[index].isComplete = !todos[index].isComplete;
    this.setState({ todos });
  };

  deleteItem = index => {
    console.log("boton selected" + "🧷");
    // this.state.todos.splice(index, 1);
    this.setState({
      todos: this.state.todos.filter((_, i) => i !== index)
    });
  };

  render() {
    return (
      <React.Fragment>
        <Container fixed>
          <Typography variant="h2" align="center" gutterBottom>
            {" "}
            To-do List
          </Typography>
          <Grid container justify="center">
            <Grid item>
              <TodoForm
                addTask={this.addTask}
                inputValues={this.inputValues}
                value={this.state.value}
              />
            </Grid>
            <Grid container justify="center">
              <Grid item md={8}>
                <Grid container justify="center">
                  <Grid item md={8}>
                    <TodoList
                      deleteItem={this.deleteItem}
                      todo={this.state.todos}
                      toggleCompleted={this.toggleCompleted}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
