import React from "react";
import List from "@material-ui/core/List";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  ListItemText,
  Checkbox,
  IconButton,
  ListItemSecondaryAction,
  ListItem
} from "@material-ui/core";

const TodoList = ({ deleteItem, todo, toggleCompleted }) => (
  <List>
    {todo.map((todo, index) => {
      return (
        <ListItem button key={index}>
          <Checkbox checked={todo.isComplete} onClick={() => toggleCompleted(index)}/>
          <ListItemText primary={todo.value} />
          <ListItemSecondaryAction >
            <IconButton onClick={() => deleteItem(index)}>
              <DeleteIcon  />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      );
    })}
  </List>
);

export default TodoList;
